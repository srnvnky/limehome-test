const API_URL = process.env.REACT_APP_API_URL as string;
const API_KEY = process.env.REACT_APP_HERE_API_KEY as string;

// it's just a simple fetch don't really want to test it for coding test
// in real-world it's make sense to test adding key and handling errors logic
export function hereApiRequest(endpoint: string) {
    const endpointURL = new URL(API_URL + endpoint);

    endpointURL.searchParams.append("apiKey", API_KEY);

    return fetch(endpointURL.toString())
        .then(r => r.json())
        .catch(e => { throw new Error(e)});
}
