// I'll type only properties that I use for simplicity
export interface IPlace {
    id: string;
    title: string;
    distance: number;
    position: [number, number];
    active?: boolean;
}
