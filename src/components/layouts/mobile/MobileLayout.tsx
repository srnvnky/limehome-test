import React, { FC } from 'react';
import './MobileLayout.scss';
import { Header } from '../../ui/header';

export const MobileLayout: FC = ({ children }) => {
    return (
        <div className="MobileLayout">
            <Header />
            <div className="MobileLayout__body">
                {children}
            </div>
        </div>
    );
}
