import React from 'react';
import { render } from '@testing-library/react';
import { MobileLayout } from './MobileLayout';

// most presentational components could be simply snapshot tested like this. I'll skip such tests for the rest
it('renders correctly', () => {
    const { baseElement } = render(<MobileLayout>content</MobileLayout>);
    
    expect(baseElement).toMatchSnapshot();
});
