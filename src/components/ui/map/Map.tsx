import React, { FC, MouseEvent } from 'react';
import './Map.scss';
import classNames from 'classnames';
import GoogleMapReact, { Props as GoogleMapProps, Position as GoogleMapsPosition } from 'google-map-react';
import HomeIcon from './home.svg';
import HomeActiveIcon from './home-active.svg';
import { IPlace } from '../../../config/types';
import { Button } from '../button';

interface IMapProps extends GoogleMapProps {
    className?: string;
    places: IPlace[];
    onPlaceClick: (place: IPlace) => void;
    selectedPlace?: IPlace | null;
}

// make sense to use Geolocation.getCurrentPosition(), but I'll keep things simple
// I'll skip the test because mostly it's third-party logic
// in real world scenario I would definitely test places rendering and handle click on place
export const Map: FC<IMapProps> = ({ className, places, onPlaceClick, selectedPlace, defaultCenter = { lat: 52.52, lng: 13.40 }, defaultZoom = 14, ...props }) => {
    return (
        <div className={classNames(["Map", className])}>
            {/* using 3rd party google maps lib for simplicity sake */}
            <GoogleMapReact
                bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY ?? "" }}
                defaultCenter={defaultCenter}
                defaultZoom={defaultZoom}
                options={{ disableDefaultUI: true, disableDoubleClickZoom: true, clickableIcons: false }}
                {...props}
            >
                {places.map((place) => (
                    <Place lat={place.position[0]} lng={place.position[1]} key={place.id} active={selectedPlace?.id === place.id} place={place} onClick={onPlaceClick} />
                ))}
            </GoogleMapReact>
        </div>
    );
}

interface IPlaceProps extends GoogleMapsPosition {
    active?: boolean;
    place: IPlace;
    onClick: (place: IPlace) => void;
}

const Place: FC<IPlaceProps> = ({ active, onClick, place }) => {
    function onSelfClick(e: MouseEvent) {
        e.stopPropagation();
        onClick(place);
    }

    return (
        <Button onClick={onSelfClick} className="Map__marker">
            <img src={active ? HomeActiveIcon : HomeIcon} className="Map__marker-icon" alt={place.title} />
        </Button>
    )
}
