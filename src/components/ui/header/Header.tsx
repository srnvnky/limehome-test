import React, { FC } from 'react';
import './Header.scss';
import { ReactComponent as MenuIcon } from './menu.svg';
import { Logo } from '../logo';
import { Button } from '../button';

export const Header: FC = () => {
    return (
        <header className="Header">
            <Logo className="Header__logo" />
            <Button className="Header__menu">
                <MenuIcon className="Header__menu-icon" />
            </Button>
        </header>
    );
}
