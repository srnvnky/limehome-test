import React, { ButtonHTMLAttributes, FC } from 'react';
import './Button.scss';
import classNames from 'classnames';

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    fullWidth?: boolean;
    variant?: 'default' | 'primary';
}

export const Button: FC<IButtonProps> = ({ children, className, type = "button", fullWidth, variant = 'default', ...props }) => {
    const buttonClassName = classNames(["Button", `_${variant}`, { '_full-width': fullWidth }, className]);

    return (
        <button type={type} className={buttonClassName} {...props}>
            {children}
        </button>
    );
}
