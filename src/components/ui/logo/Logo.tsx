import React, { FC, SVGAttributes } from 'react';
import { ReactComponent as LogoSvg } from './logo.svg';

interface ILogoProps extends SVGAttributes<SVGElement> {}

export const Logo: FC<ILogoProps> = (props) => {
    return (
        <LogoSvg {...props} />
    );
}
