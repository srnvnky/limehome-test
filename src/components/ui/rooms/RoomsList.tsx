import React, { FC } from 'react';
import './RoomsList.scss';
import classNames from 'classnames';
import { Button } from '../button';
import { IPlace } from '../../../config/types';

interface IHotelsListProps {
    className?: string;
    hotel: IPlace;
    rooms: any[];
}

// tbh list in the bottom might be a list of hotels rather than list of rooms in the selected hotel, but I'll assume the latter since it's way easier
export const RoomsList: FC<IHotelsListProps> = ({ className, hotel, rooms }) => {
    return (
        <div className={classNames(["RoomsList", className])}>
            {rooms.length === 0 && (
                <p className="RoomsList__empty">No rooms available.</p>
            )}
            {rooms.map((_, index) => (
                // using index as a key since we don't have rooms info. ideally it have to be some id
                <RoomListItem key={index} single={rooms.length === 1} hotel={hotel} />
            ))}
        </div>
    );
}

interface IRoomsListItemProps {
    single?: boolean;
    hotel: IPlace;
}

// if we not gonna use it anywhere else, it's fine to keep it here
const RoomListItem: FC<IRoomsListItemProps> = ({ single, hotel }) => {
    return (
        <div className={classNames(["RoomListItem", { _single: single }])}>
            <div className="RoomListItem__info">
                <figure className="RoomListItem__image-wrapper">
                    <img
                        src="https://cf.bstatic.com/images/hotel/max1024x768/236/236639618.jpg"
                        alt="room"
                        className="RoomListItem__image"
                    />
                </figure>
                <div className="RoomListItem__info-text">
                    <h5 className="RoomListItem__title">{hotel.title}</h5>
                    <p className="RoomListItem__distance">{(hotel.distance / 1000).toFixed(1)} km from the city center</p>
                    <p className="RoomListItem__price">$98</p>
                    <p className="RoomListItem__note">Designs may vary</p>
                </div>
            </div>
            <Button fullWidth variant="primary" className="RoomListItem__book">Book</Button>
        </div>
    )
}
