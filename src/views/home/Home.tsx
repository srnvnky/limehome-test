import React, { FC, useEffect, useState } from 'react';
import './Home.scss';
import { Map } from '../../components/ui/map';
import { RoomsList } from '../../components/ui/rooms';
import { ChangeEventValue } from 'google-map-react';
import { hereApiRequest } from '../../utils/hereApiRequest';
import { IPlace } from '../../config/types';

const MAX_HOTELS = 50;
const ROOMS_MOCK = [{}, {}, {}, {}, {}];

// since there are many third party stuff, I'm not quite sure that it's make much sense to test it using jest (you would basically test your mocks)
// and probably some e2e test (using selenium or cypress) will fit better for such logic
export const Home: FC = () => {
    const [hotels, setHotels] = useState<IPlace[]>([]);
    const [selectedHotel, setSelectedHotel] = useState<IPlace | null>(null);
    const [rooms, setRooms] = useState<any[]>([]);

    useEffect(() => {
        if (!hotels.find(h => h.id === selectedHotel?.id)) {
            setSelectedHotel(null);
        }
    }, [hotels, selectedHotel]);

    async function onMapMove(value: ChangeEventValue) {
        try {
            // let's assume distance returned from this api is the distance from the center of city, rather than distance from "at" param
            // otherwise I would have to determine the location based on data.search.context.location, get it's coordinates and calculate them for each place
            const data = await hereApiRequest(`/discover/search?at=${value.center.lat},${value.center.lng}&q=hotel&size=${MAX_HOTELS}`);

            // I'll skip validation and assume I got IPlace[] in data.results.items to keep things simple
            setHotels(data.results.items);
        } catch (e) {
            // handle error
        }
    }

    function selectHotel(hotel: IPlace) {
        setSelectedHotel(hotel);
        // just mocked data, should be fetch based on selected hotel
        setRooms(ROOMS_MOCK.slice(0, Math.round(Math.random() * ROOMS_MOCK.length)));
    }

    return (
        <div className="Home">
            {/* Maybe it's make sense to throttle onMapMove a bit */}
            <Map className="Home__map" onChange={onMapMove} places={hotels} onPlaceClick={selectHotel} selectedPlace={selectedHotel} />
            {selectedHotel && (
                <RoomsList hotel={selectedHotel} rooms={rooms} className="Home__rooms" />
            )}
        </div>
    );
}
