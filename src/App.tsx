import React from 'react';
import { MobileLayout } from './components/';
import { Home } from './views/home';

export function App() {
    return (
        // skip BrowserRouter -> Switch -> Route, since we only have single page
        <MobileLayout>
            <Home />
        </MobileLayout>
    );
}
